extract_rates;

fid = fopen('inputt.txt')
offset = 30
nwfs = 5*4;
Ngrid = str2num(fgetl(fid));
for ctr = 1:offset-1
    fline = fgetl(fid); 
end
Ens = 0
for ctr = 1:nwfs
fline = fgetl(fid);
enline = str2num(fline);
Ens(ctr)= enline(1);
end
psi = zeros(nwfs,Ngrid);
xs  =  zeros(nwfs,Ngrid);

% now read the wavefunctions
for wf=1:nwfs
    for ctr = 1:Ngrid
        fline = fgetl(fid);
        xpsi = str2num(fline);
        xs(wf,ctr) = xpsi(1);
        psi(wf,ctr) = xpsi(2);
    end
end

fclose(fid);


% extract  the energies in the following order 
% Injector1, Injector2 -> ULL -> LLL1 -> LLL2 -> DEPOP1 -> DEPOP2
% DEPOP 
% these indices are of the WFs with energies ordered in increasing order!
% BUILD THE TUNNELING HAMILTONIAN
INJ1 = 12; INJ2 = 10; ULL = 11; LLL1=9; LLL2=8; DEPOP1 = 7;DEPOP2 = 5;
lvls = [INJ1 INJ2 ULL LLL1 LLL2 DEPOP1 DEPOP2]; 

[sortedEns, order]  = sort(Ens);
Energy_lvls = sortedEns(lvls);
levs2get = order(lvls) 


fid = fopen('inputt.txt')
offset = 18450
for ctr = 1:offset
    fline = fgetl(fid); 
end



TUNTIMES = zeros(nwfs,nwfs);
ENDIFF = zeros(nwfs,nwfs);
for ctr = 1:nwfs
fline = fgetl(fid);
data = str2num(fline); 
TUNTIMES(ctr,1:end) = data(1:2:end);
ENDIFF(ctr,1:end) = data(2:2:end);
end

NLvls = length(levs2get); 

e0=1.60217646e-19; hbar=1.05457168e-34;
hbar = hbar/e0; % in eV s

HTB_mtx = zeros(length(levs2get),length(levs2get)); 
for i = 1:NLvls
    for j = 1:NLvls
        if i == j
            HTB_mtx(i,i) = Energy_lvls(i);
        else
            HTB_mtx(i,j) = TUNTIMES(levs2get(i),levs2get(j))*hbar;
        end
    end
end

HTB_lin = reshape(HTB_mtx,[1,NLvls*NLvls]);
deph_times_INJ_ULL = gam(levs2get(2),levs2get(3),pwd,0);
deph_times_ULL_LLL = gam(levs2get(3),levs2get(4),pwd,0);
deph_times_INJ_LLL = gam(levs2get(2),levs2get(4),pwd,0);

pure_deph_INJ_ULL = 1e12*1/deph_times_INJ_ULL(end)
pure_deph_ULL_LLL = 1e12*1/deph_times_ULL_LLL(end)
pure_deph_INJ_LLL = 1e12*1/deph_times_INJ_LLL(end)

display(W_sf');
display(HTB_lin);




