       
      program struct_2
!*********************************************************************
!
!You have to set the various parameters and write the dimension
!of the various layers at the beginning of the executable programm
!
!*******************************************************************************

integer, parameter :: MC_5reg=0          !=1 makes an input file for 
                                         !   a MC which uses 5 periods
                                         !   (act_inj_act_inj_act)  
                                         !=0 makes an input file for
										 !   a MC which uses 2 periods
										 !	 (inj_act)
integer, parameter :: temp= 50           ! temperature in K 


!********************** structure parameters ************************************
 
integer, parameter :: Si_SiGe=0			   ! if =1 it is Si/SiGe       
integer, parameter :: GaAs_AlAs=0          ! if =1 it is GaAs/AlAs
integer, parameter :: GaAs_ALGaAs=0        ! if =1 it is GaAs/ALGaAs
integer, parameter :: InGaAs_InAlAs=0      ! if =1 it is InGaAs/InAlAs 
integer, parameter :: Multimaterial=1      ! if =1 it is a structure containning more then 2type of heterojunctions
                                           ! this is a custom matarial system, always change all the parameters for the mat.
                                           

REAL(kind=kind(1.d0)), parameter :: conc_w=0.0 !mole fraction of Ga for In(1-x)Ga(x)As
REAL(kind=kind(1.d0)), parameter :: conc_b=0.15 !of Al for GaAs/ALGaAs
                                                !of Al for Al(x)In(1-x)As
												!of Ge for SiGe
												

!Parameters for the MC

REAL(kind=kind(1.d0)), parameter ::cob=0.135     !conduction band gamma offset, sometimes also 66% offset assumed
REAL(kind=kind(1.d0)), parameter ::cob1=0.0  !conduction band L offset
REAL(kind=kind(1.d0)), parameter ::cob2=0.0 !conduction band hh offset
REAL(kind=kind(1.d0)), parameter ::cob3=0.0 !conduction band lh offset
! do not touch material specific parameters (temp dependece of the energy gap fo rGaAs)
REAL(kind=kind(1.d0)), parameter ::EgGaAs=1.519-0.5405e-3*temp**2/(temp+204);! temperature dependent bandgaps from Vurgaftman database
REAL(kind=kind(1.d0)), parameter ::EgInAs=0.417-0.276e-3*temp**2/(temp+94);
REAL(kind=kind(1.d0)), parameter ::EgAlAs=3.099-0.885e-3*temp**2/(temp+530);
REAL(kind=kind(1.d0)), parameter ::DsoGaAs=0.341;     ! splitoff energy
REAL(kind=kind(1.d0)), parameter ::DsoInAs=0.39;
REAL(kind=kind(1.d0)), parameter ::DsoAlAs=0.28;
REAL(kind=kind(1.d0)), parameter ::FGaAs=-1.94;
REAL(kind=kind(1.d0)), parameter ::FInAs=-2.9;
REAL(kind=kind(1.d0)), parameter ::FAlAs=-0.48;
REAL(kind=kind(1.d0)), parameter ::EpGaAs=28.8;
REAL(kind=kind(1.d0)), parameter ::EpInAs=21.5;
REAL(kind=kind(1.d0)), parameter ::EpAlAs=21.1;

!**************************************************************************
!offset dependent on mole fraction would be nice
!   perc=conc_b
!   cob=(1.155*perc+0.37*perc**2)*0.65	!cob-gamma
!   cob_L=(1.155*perc+0.37*perc**2)*0.65	!cob-L		formula only valid
!   cob_hh=(1.155*perc+0.37*perc**2)*0.35	!cob-hh		for AlGaAs!!!
!   cob_lh=(1.155*perc+0.37*perc**2)*0.35	!cob-lh
!**************************************************************************

REAL(kind=kind(1.d0)), parameter ::fmin=0.0   !RANGE transverse el.mag.field (parall
REAL(kind=kind(1.d0)), parameter ::fmax=0.0   !to the interface), normalized at 0
        

REAL(kind=kind(1.d0)), parameter ::bz=0.001      !not make B field absolutely zero 
                                                 !(in tesla)

REAL(kind=kind(1.d0))::phi_fin
        
REAL(kind=kind(1.d0)), parameter ::dens_m_2=3.078e14   ! set the charge density in m^-2 <- sheet doping density! = avg volume density x period length
REAL(kind=kind(1.d0)), parameter ::dtsim=1.0e-14 !dt simulation time step
REAL(kind=kind(1.d0)), parameter ::tsimax=7.0e-11 !simulation end time
REAL(kind=kind(1.d0)), parameter ::tstat=7.0e-11! !the time to reachstationary state 
     
integer ll_high       !upper laser level
integer ll_low        !lower laser level
integer linj_inter    !injector level of interest
integer lreg_act__low !active region ground level 
integer nbnd          !number of considered bands
integer nperiod(4)
integer nlayers       !layers per period
      

!parameter for the e-e scattering, don't change!
REAL(kind=kind(1.d0)), parameter ::ceemax=1.0e14


!FLAGS FOR THE MC SIMULATIONS do not touch!!!
integer, parameter ::nrtyp=1       !1 electron simulation; 3 hole simulation
integer, parameter ::jee=2         !e-e interaction(1 yes,0 no)
integer, parameter ::jgamma=1	   !gamma valley simulation(1 yes, 0 no)
integer, parameter ::jho=0         !hole simulation (1 yes, 0 no)
integer, parameter ::jlv=0         !l-valley electrons simulation (1 yes, 0 no)
integer, parameter ::jlh=0		   !light hole simulation (1 yes, 0 no)
integer, parameter ::jhp=1         !hot phonons(1 yes, 0 no)
integer, parameter ::jdeg=1        !degeneracy(1 yes, 0 no)
integer, parameter ::jprdf=1       !print electron distribution function(1 yes, 0 no)
integer, parameter ::jprpdf=1      !print phonon distribution(1 yes,0 no)
integer, parameter ::jprsr=1       !print scattering rates(1 yes,0 no)
integer, parameter ::jlsr=0        !laser excitation(1 yes,0 no)
integer, parameter ::jread=1       !read wavefunctions from SHP-input file (1 yes, 0 no)
integer, parameter ::jcorrente=1   !fixed current (1 no, 0 yes)
integer, parameter ::jinitial_cond=0  !1=read the file with initial condition
integer, parameter ::jread_ff=0    !=0 calculates the form factors, =1 reads them(not possible at first run!)
integer, parameter ::jIFphonon=0   !IF(interface) phonon (1 yes, 0 no)
integer, parameter ::jrecalc=0     !update subbands and scattering(1 yes,0 no) 

!********************************************************************************


!********* These parameters can be usually left as follows **********************
!not used for calculations!        
REAL(kind=kind(1.d0)), parameter :: interv_ener_min=25.137d0 !Limits of the allowed
REAL(kind=kind(1.d0)), parameter :: Gamma=10.0e-9 !interface roughness corr. length in m
REAL(kind=kind(1.d0)), parameter :: Delta=0.12e-9 !0.08e-9 !interface roughness height in m Do not touch 
REAL(kind=kind(1.d0)), parameter :: vett_fon_max=0.1    !phonon wave vectors in 1/A
REAL(kind=kind(1.d0)), parameter :: sr_energ_max=0.1    !sr_max for scat rates (eV)
!REAL(kind=kind(1.d0)), parameter :: accettori=0.0d0     !Concentration of accpetors and used for calculations!
REAL(kind=kind(1.d0)), parameter :: donori=1.9e-8         ! VOLUME DENSITY in units A^-3 (average number of carriers per unit volume (in angstrom))
REAL(kind=kind(1.d0)), parameter :: donoriSL=0.0d0      !not needed anymore
REAL(kind=kind(1.d0)), parameter :: phifin=11d0       !SET BIAS Pot. or field at end of the structure (Bias in kV/cm)
		
!*********************************************************************************
!****************        END OF SETTINGS      ************************************
!********************************************************************************* 
 
 
REAL(kind=kind(1.d0)) :: phiniz=0.0d0        !Potential applied at beginning in V
integer fin_reg,i,i_inj,i_rega,lungh_regact
integer fine_rega,fine_dop

REAL(kind=kind(1.d0)), DIMENSION(200) :: lung_reg_act_layer,lung_reg_act_layer1
REAL(kind=kind(1.d0)), DIMENSION(200) :: coord_reg

real(kind=kind(1.d0)), dimension(7) :: band_offset !conduction band offset,effective mass, bandgap 
real(kind=kind(1.d0)), dimension(7) :: m_eff       ! and the following 1 parameter 
real(kind=kind(1.d0)), dimension(7) :: bandgap     ! are only used if Multimaterial=1 
integer, dimension(200) :: material_select_reg_act_layer !assigns material to layer


integer, Dimension(4) :: nsub
integer, DIMENSION(4,50) ::indw
integer, dimension(100) :: doping
integer stato_dopato,nsubmx,nper,provv
real dope(100,4),conc_Al(7)
!***************************************************************************

!----------------------------------------------------- 
!    Implicit function declaration
!----------------------------------------------------- 

REAL(kind=kind(1.d0)), INTRINSIC :: atan,sqrt,mod,dexp 
  
!----------------------------------------------------- 
if(jlh==1)phiniz=0.03d0
nperiod=(/5,0,0,0/); nper=5; !nper stands for number of spatial periods to include in the simulation nperiod -> specifies the number 
! of wavefuncts per period
nlayers=9;             ! number of the layers does not necessarily correspond to nr of layers in the active region due 
!to d-doping layer splitting 
nbnd=0
if (jgamma.eq.1) then 
nbnd=nbnd+1 
end if
if (jlv.eq.1) then
nbnd=nbnd+1
end if
if (jho.eq.1) then
nbnd=nbnd+1
end if
if (jlh.eq.1) then
nbnd=nbnd+1
end if
     
!Choose here the energy levels used in the MC-simulation
       				
    do i=1,50
		indw(1,i)=i
    end do
	
 nsubmx=4*max(nperiod(1),nperiod(2),nperiod(3),nperiod(4))
       
!Among the energy levels chosen above we indicate the upper and lower laser level,
!the depopulation level of the active region and the injector level of interest
! PeTz: DO not chage! Does not play a role in the simulation
	   
       ll_high=6        !upper laser level
       ll_low=5        !lower laser level
       linj_inter=4     !injector level of interest
       lreg_act__low=3  !active region ground level
       lreg_act_high=3 !highest active region energy level
!***************************************************************************
!***************************************************************************

        Open(unit=2001,file="struct_2.inp",status="unknown")
        Open(unit=2002,file="shp_data.inp",status="unknown")
      

!     LENGTH OF THE ACTIVE REGION LAYERS (always with the injection and extraction barrier): start with barrier

if (Multimaterial==1) then

!!!! Achtung der Band_offset ist hier ge�ndert  !!!!!!!


! Katz structure
!
! material 1: Al(x)GaAs(1-x)
! material 2: AlInAs
! material 3: GaInAs
! material 4: InAs 

! conduction band offset of material 1,2,...      


 ! layer index:  1     2    3     4    5     6     7 -> dont care 
       conc_Al=(/0.15, 0.0, 0.15, 0.3, 0.15, 0.15, 0.0/)

do i=1,2
   band_offset(i)=0.9*conc_Al(i)
   m_eff(i)=conc_Al(i)/(1+2*FAlAs+EpAlAs*(EgAlAs+2*DsoAlAs/3)/EgAlAs/(EgAlAs+DsoAlAs));
   m_eff(i)=m_eff(i)+(1-conc_Al(i))/(1+2*FGaAs+EpGaAs*(EgGaAs+2*DsoGaAs/3)/EgGaAs/(EgGaAs+DsoGaAs));  ! Temperature dependent effective mass of material 1,2,...  
   bandgap(i)=EgGaAs*(1-conc_Al(i))+EgAlAs*conc_Al(i)+conc_Al(i)*(1-conc_Al(i))*0.127 !1.5930   ! bandgap of material 1,2,...! set to large value to neglect nonparabolicity
end do
   !m_eff(1)=0.075555
   !m_eff(2)=0.067
   
   ! PeTz: in the following define the active region layers     
   do i=0,nper-1 
       
		lung_reg_act_layer(1+nlayers*i)=24                      !barrier
		material_select_reg_act_layer(1+nlayers*i)=1         		
		lung_reg_act_layer(2+nlayers*i)=92                  !well 
		material_select_reg_act_layer(2+nlayers*i)=2		
		lung_reg_act_layer(3+nlayers*i)=33                  !barrier
    	material_select_reg_act_layer(3+nlayers*i)=1         
    	
    	lung_reg_act_layer(4+nlayers*i)=162                 !well - doped part 
    	material_select_reg_act_layer(4+nlayers*i)=2         
    	
       
    	lung_reg_act_layer(5+nlayers*i)=40                  !barrier
    	material_select_reg_act_layer(5+nlayers*i)=1   
    	lung_reg_act_layer(6+nlayers*i)=73                  !well
    	material_select_reg_act_layer(6+nlayers*i)=2        		
		lung_reg_act_layer(7+nlayers*i)=23                     !barrier
		material_select_reg_act_layer(7+nlayers*i)=1        
		lung_reg_act_layer(8+nlayers*i)=76                 !well 
		material_select_reg_act_layer(8+nlayers*i)=2	   
		lung_reg_act_layer(9+nlayers*i)=24               !barrier <- this is the additional barrier 
        material_select_reg_act_layer(9+nlayers*i)=1        

    	
    end do
    i=nper; !PeTz: add another barrier at the end of the structure to confine the WFs. Do not change! 
        lung_reg_act_layer(1+nlayers*i)=lung_reg_act_layer(1)               !barrier, must always end with the total ammount of barrier used
        material_select_reg_act_layer(1+nlayers*i)=material_select_reg_act_layer(1);
		fine_rega=1+nlayers*nper !Determines # of layers in the active region (fine_rega)


	!Below the doped layers of the injector/active region are determined, which are used in the MC to calculate the current
    !doping(1)=4 means that the first doped layer is equal to the fourth layer
   do i=0,nper-1
        doping(1+1*i)=4+nlayers*i	
  end do
   
   provv=sum(lung_reg_act_layer(doping(1:nper)));
   else
   
      do i=0,nper-1
        lung_reg_act_layer(1+nlayers*i)=15             
        lung_reg_act_layer(2+nlayers*i)=0    
        lung_reg_act_layer(3+nlayers*i)=12  
        lung_reg_act_layer(4+nlayers*i)=0  
        lung_reg_act_layer(5+nlayers*i)=15                 !barrier  
        lung_reg_act_layer(6+nlayers*i)=85                  !well
        lung_reg_act_layer(7+nlayers*i)=28                 !barrier
        lung_reg_act_layer(8+nlayers*i)=85                  !well
		lung_reg_act_layer(9+nlayers*i)=48                 !barrier
		lung_reg_act_layer(10+nlayers*i)=164                  !well 
    end do

	!i=nper;

        !lung_reg_act_layer(1+nlayers*i)=lung_reg_act_layer(1)                 !barrier, must always end with the total ammount of barrier used
		fine_rega=nlayers*i !Determines # of layers in the active region (fine_rega)


	!Below the doped layers of the injector/active region are determined, which are used in the MC to calculate the current
    !doping(1)=4 means that the first doped layer is equal to the fourth layer
  do i=0,nper-1
		!doping(1+i)=8+nlayers*i
		doping(1+i)=10+nlayers*i
   end do
     
   provv=sum(lung_reg_act_layer(doping(1:nper)));
end if
!******************************************************************
!******************************************************************
!******************************************************************
!***********			 END OF SETTINGS			  *************
!******************************************************************
!******************************************************************
!******************************************************************
		!Calculation of a parameter that gives the thickness of the doped zone


        write(2002,*) fine_rega			         ! writes number of layers to s-p solver
        
8000    format(F6.2,3x,F14.12,3x,F14.12,3x,e14.7)        

        if (Multimaterial==1) then
              do i=1,fine_rega
              write(2002,8000) lung_reg_act_layer(i), band_offset(material_select_reg_act_layer(i)),&
                            & m_eff(material_select_reg_act_layer(i)),bandgap(material_select_reg_act_layer(i)),0,0
              end do
              write(2002,*), nper ! write number of spatial periods
        else  
              do i=1,fine_rega 
              write(2002,*) lung_reg_act_layer(i)   ! writes layers to schrodinger-poisson solver
              end do
        end if
        

		write(2002,*) jgamma,jlv,jho,jlh        !doping concentration for the Schroedinger-Poisson
        write(2002,*) donori,donoriSL	        !in injector and active region
        write(2002,*) nbnd
		if (jgamma.eq.1) then
		write(2002,*) cob	!conduction band offsets
        end if
		if (jlv.eq.1) then 
		write(2002,*) cob1
		end if
		if (jho.eq.1) then
		write(2002,*) cob2
		end if
		if (jlh.eq.1) then
		write(2002,*) cob3
		end if

     num_eterog=0	!total number of heterostructures in structure
     fin_reg=0		!total number of layers in one active region(=fine_rega)
     lungh_regact=0	!total length of one active region
     lung_reg_act_layer1=0 ! initialize vector that contains the structure itself without 0 layer lengths 
     
        k=1;
        do i=1,nper*nlayers+1
          lungh_regact=lungh_regact+lung_reg_act_layer(i)
          if (lung_reg_act_layer(i)/=0) then
          lung_reg_act_layer1(k)=lung_reg_act_layer1(k)+lung_reg_act_layer(i)
          if ((i<nper*nlayers+1).and.(lung_reg_act_layer(i+1)/=0)) k=k+1
          end if
        end do        
        
        coord_reg(1)=-lungh_regact;
        do i=1,k-1
        coord_reg(i+1)=coord_reg(i)+lung_reg_act_layer1(i)
        end do

        do i=1,nper*nlayers+1

          if(lung_reg_act_layer(i)==0)then
             fin_reg=fin_reg-1             
          else
             fin_reg=fin_reg+1
          end if
         end do
									
!***calculate coordinates for each layer; including 0 length layers, for calculating the right amount of doping *****
  
!***************************************************************************
!*********calculate limits of doped regions and concentration***************
	
	do fine_dop=1,50
           if(doping(fine_dop+1).eq.0) exit
    end do

    dope(1:fine_dop,4)=1; !for electron donors this is 1
    do i=1,nper
                dope(i,1)= sum(lung_reg_act_layer(1:(doping(1+(i-1)*fine_dop/nper))-1))
                dope(i,2)= sum(lung_reg_act_layer(1:doping(fine_dop/nper+(i-1)*fine_dop/nper)))
                dope(i,3)= donori*1.e24
	end do
    fine_dop=nper;
!******** Writes total #of layers and all interface coordinates ************
        write(2001,*) (fine_rega-1)
        write(2001,*) (coord_reg(i),i=2,fin_reg)
!** Save the CONCENTRATIONS (of Ga for In(1-x)Ga(x)As, Al for GaAs/ALGaAs,***
!************************* of Al for Al(x)In(1-x)As)  ***********************
        
        do i=1,(fin_reg)
        write(2001,*) conc_Al(material_select_reg_act_layer(i))
        end do

!*********************  Save z_min and z_max (z=growth direction)****
         write(2001,*) -lungh_regact,0.0d0
!***** interface roughness parameters (originally limits of the allowed phonon energy in meV) ************** 
         write(2001,*) Delta,Gamma
!*************************************************************** 

!**** limits of the allowed phonon wave vectors in 1/A********** 
        write(2001,*) nper,vett_fon_max
!*************************************************************** 

!***  sr_max for scat rates (eV) *****************************  
        write(2001,*) sr_energ_max 
!***************************************************************

!***  temperature in K   ***************************************
        write(2001,*) temp
!***************************************************************

!*** # of wells to simulate ************************************
        write(2001,*) (fin_reg-1)/2
!***************************************************************

!*** Doping ****************************************************
        write(2001,*) donoriSL,donori	
!***************************************************************

!*** relative permittivity *************************************
        if((GaAs_AlAs==1).or.(GaAs_ALGaAs==1))then
           write(2001,*) 12.3
           write(2002,*) 12.3
        end if

        if(InGaAs_InAlAs==1)then     
           write(2001,*) 15.15 - 2.24*conc_w
           write(2002,*) 15.15 - 2.24*conc_w
        end if
        
        if (Multimaterial==1) then
           write(2001,*) 12.3
           write(2002,*) 12.3
        end if

		if(Si_SiGe==1)then     
           write(2001,*) 12.825
           write(2002,*) 12.825
        end if
!***************************************************************

        phi_fin=phifin*lungh_regact*1.0d-5+phiniz

        print*, phi_fin , ' Applied Field in Volt'

!***  Potential at the beginning and the end of the structure****
        write(2001,*) phiniz
        write(2001,*) phi_fin
        write(2002,*) phiniz
        write(2002,*) phi_fin
!***************************************************************
 
		write(2001,*) nrtyp,jee,jgamma,jho,jlv,jlh,jhp,jdeg,jprdf,jprpdf,jprsr,jlsr

!*** effective mass and factor of non-parabolicity***************
        
       if(GaAs_ALGaAs==1)then
          if(jgamma==1) then 
		   write(2001,*) 1/(1+2*FGaAs+EpGaAs*(EgGaAs+2*DsoGaAs/3)/EgGaAs/(EgGaAs+DsoGaAs)) !0.0657+0.0174*conc_w+0.145*conc_w**2     ! effective mass in well
           write(2001,*) conc_b/(1+2*FAlAs+EpAlAs*(EgAlAs+2*DsoAlAs/3)/EgAlAs/(EgAlAs+DsoAlAs))+(1-conc_b)/(1+2*FGaAs+EpGaAs*(EgGaAs+2*DsoGaAs/3)/EgGaAs/(EgGaAs+DsoGaAs)) !0.0657+0.0174*conc_b+0.145*conc_b**2	  ! effective mass in barrier
           write(2001,*) 4.9e-19
		     ! factor of non-parabolicity in well and barrier ( (hbar*hbar)/(2*m*Eg)) Eg- energy gap (in case of well) or energy gap + band offset (in case of AlGaAs barrier)
           write(2002,*) 1/(1+2*FGaAs+EpGaAs*(EgGaAs+2*DsoGaAs/3)/EgGaAs/(EgGaAs+DsoGaAs)) !0.0657+0.0174*conc_w+0.145*conc_w**2     ! effective mass in well
           write(2002,*) conc_b/(1+2*FAlAs+EpAlAs*(EgAlAs+2*DsoAlAs/3)/EgAlAs/(EgAlAs+DsoAlAs))+(1-conc_b)/(1+2*FGaAs+EpGaAs*(EgGaAs+2*DsoGaAs/3)/EgGaAs/(EgGaAs+DsoGaAs)) !0.0657+0.0174*conc_b+0.145*conc_b**2	  ! effective mass in barrier
!           write(2002,*) 3.80998081e-20/(0.0657+0.0174*conc_w+0.145*conc_w**2)/1.42,3.80998081e-20/(0.0657+0.0174*conc_b+0.145*conc_b**2)/(1.42+1.594*conc_b+conc_b*(1-conc_b)*(0.127-1.310*conc_b))
!           write(2002,*) 3.80998081e-20/(0.0657+0.0174*conc_w+0.145*conc_w**2)/1.42,3.80998081e-20/(0.0657+0.0174*conc_w+0.145*conc_w**2)/1.42
           write(2002,*) 4.9e-19,3.9e-19

		  endif
		  if (jlv==1) then 
		   write(2001,*) 0.22                ! effective mass in well(L)
           write(2001,*) 0.25				  ! effective mass in barrier(L)
           write(2001,*) 4.9E-19              ! factor of non-parabolicity
		   write(2002,*) 0.22                ! effective mass in well
           write(2002,*) 0.25				  ! effective mass in barrier
           write(2002,*) 4.9E-19              ! factor of non-parabolicity
		  endif
		  if (jho==1) then
		   write(2001,*) 0.417                ! effective mass in well(hh)
           write(2001,*) 0.44				  ! effective mass in barrier(hh)
           write(2001,*) 4.9E-19              ! factor of non-parabolicity
		   write(2002,*) 0.417                ! effective mass in well
           write(2002,*) 0.44				  ! effective mass in barrier
           write(2002,*) 4.9E-19              ! factor of non-parabolicity
		  endif
		  if (jlh==1) then 
		   write(2001,*) 0.107                ! effective mass in well(lh)
           write(2001,*) 0.15				  ! effective mass in barrier(lh)
           write(2001,*) 4.9E-19              ! factor of non-parabolicity 
		   write(2002,*) 0.107                ! effective mass in well
           write(2002,*) 0.15				  ! effective mass in barrier
           write(2002,*) 4.9E-19              ! factor of non-parabolicity
          endif
		endif

        if(InGaAs_InAlAs==1)then     
          if(jgamma==1) then 
		   write(2001,*) 0.067*(1-conc_w)+0.023*conc_w-0.0091*conc_w*(1-conc_w)
           write(2001,*) 0.023*conc_b+0.15*(1-conc_b)-conc_b*(1-conc_b)*0.049;  ! effective mass in barrier
           write(2001,*) 4.9e-19    ! factor of non-parabolicity in well and barrier ( (hbar*hbar)/(2*m*Eg)) Eg- energy gap (in case of well) or energy gap + band offset (in case of AlGaAs barrier)
           write(2002,*) 0.067*(1-conc_w)+0.023*conc_w-0.0091*conc_w*(1-conc_w)
           write(2002,*) 0.023*conc_b+0.15*(1-conc_b)-conc_b*(1-conc_b)*0.049;  ! effective mass in barrier
           write(2002,*) 7.61996163e-20/(0.067-0.0501*conc_w+0.0091*conc_w**2)/(3.099-3.159*conc_w+0.477*conc_w**2),7.61996163e-20/(0.15-0.173*conc_b+0.049*conc_b**2)/(1.519-1.703*conc_b+0.7*conc_b**2) ! well nonparabolicity underestimated (factor of 3.5) for convergency reasons
		  endif
		  if (jlv==1) then 
		   write(2001,*) 0.22                ! effective mass in well(L)
           write(2001,*) 0.25				  ! effective mass in barrier(L)
           write(2001,*) 4.9E-19              ! factor of non-parabolicity
		   write(2002,*) 0.22                ! effective mass in well
           write(2002,*) 0.25				  ! effective mass in barrier
           write(2002,*) 4.9E-19              ! factor of non-parabolicity
		  endif
		  if (jho==1) then
		   write(2001,*) 0.417                ! effective mass in well(hh)
           write(2001,*) 0.44				  ! effective mass in barrier(hh)
           write(2001,*) 4.9E-19              ! factor of non-parabolicity
		   write(2002,*) 0.417                ! effective mass in well
           write(2002,*) 0.44				  ! effective mass in barrier
           write(2002,*) 4.9E-19              ! factor of non-parabolicity
		  endif
		  if (jlh==1) then 
		   write(2001,*) 0.107                ! effective mass in well(lh)
           write(2001,*) 0.15				  ! effective mass in barrier(lh)
           write(2001,*) 4.9E-19              ! factor of non-parabolicity 
		   write(2002,*) 0.107                ! effective mass in well
           write(2002,*) 0.15				  ! effective mass in barrier
           write(2002,*) 4.9E-19              ! factor of non-parabolicity
          endif
		endif
		
	   if(Multimaterial==1)then     
          if(jgamma==1) then 
           write(2001,*) 0.0653*(1-conc_w)+0.022*conc_w-0.0091*conc_w*(1-conc_w)
           write(2001,*) 0.022*conc_b+0.15*(1-conc_b)-conc_b*(1-conc_b)*0.049;  ! effective mass in barrier
           write(2001,*) 4.9e-19    ! factor of non-parabolicity in well and barrier ( (hbar*hbar)/(2*m*Eg)) Eg- energy gap (in case of well) or energy gap + band offset (in case of AlGaAs barrier)
           write(2002,*) 0.0653*(1-conc_w)+0.022*conc_w-0.0091*conc_w*(1-conc_w)
           write(2002,*) 0.022*conc_b+0.15*(1-conc_b)-conc_b*(1-conc_b)*0.049;  ! effective mass in barrier
           write(2002,*)  0*((1.519*(1-conc_w)+0.417*conc_w-conc_w*(1-conc_w)*0.477)),0*((3.099*(1-conc_b)+0.417*conc_b-conc_b*(1-conc_b)*0.7)) 
           !7.61996163e-20/(0.067-0.0501*conc_w+0.0091*conc_w**2)/(3.099-3.159*conc_w+0.477*conc_w**2),7.61996163e-20/(0.15-0.173*conc_b+0.049*conc_b**2)/(1.519-1.703*conc_b+0.7*conc_b**2) ! well nonparabolicity underestimated (factor of 3.5) for convergency reasons

		endif
	   end if

! new codelines added for Si(.75)Ge(.25) heavy holes
        if(Si_SiGe==1)then     
          if(jgamma==1) then 
		   write(2001,*) 0.47				  ! SiGe effective mass(gamma) in well (z-direction)
           write(2001,*) 0.537				  ! Si   effective mass(gamma) in barrier (z-direction)
           write(2001,*) 4.9E-19              ! factor of non-parabolicity
           write(2002,*) 0.47				  ! effective mass in well
           write(2002,*) 0.537			      ! effective mass in barrier
           write(2002,*) 4.9E-19			  ! factor of nonparabolicity
		  endif
		  if (jlv==1) then 
		   write(2001,*) 0.12				  ! SiGe effective mass(L) in well (z-direction)
           write(2001,*) 0.153				  ! Si   effective mass(L) in barrier (z-direction)
           write(2001,*) 4.9E-19              ! factor of non-parabolicity 
		   write(2002,*) 0.12				  ! effective mass in well
           write(2002,*) 0.153			      ! effective mass in barrier
           write(2002,*) 4.9E-19              ! factor of non-parabolicity
		  endif
		  if (jho==1) then 
		   write(2001,*) (0.537-0.253*conc_w)*0.55				  ! SiGe effective mass(hh) in well (z-direction)
           write(2001,*) (0.537-0.253*conc_b)*0.55				  ! Si   effective mass(hh) in barrier (z-direction)
           write(2001,*) 0              ! factor of non-parabolicity 
		   write(2002,*) (0.537-0.253*conc_w)*0.55				  ! effective mass in well
           write(2002,*) (0.537-0.253*conc_b)*0.55			      ! effective mass in barrier
           write(2002,*) 0			  ! factor of nonparabolicity
		  endif
		  if (jlh==1) then 
		   write(2001,*) (0.153-0.110*conc_w)*1.0				  ! SiGe effective mass(lh) in well (z-direction)
           write(2001,*) (0.153-0.110*conc_b)*1.0				  ! Si   effective mass(lh) in barrier (z-direction)
           write(2001,*) 0              ! factor of non-parabolicity 
		   write(2002,*) (0.153-0.110*conc_w)*1.0				  ! effective mass in well
           write(2002,*) (0.153-0.110*conc_b)*1.0			      ! effective mass in barrier
           write(2002,*) 0              ! factor of non-parabolicity
		  endif
	    endif
!***************************************************************

!*** Material system *********************************************
		if((Si_SiGe==1)) then
		   write(2001,*) 4	
		end if

        if((GaAs_AlAs==1))then
           write(2001,*) 3
        end if

        if((GaAs_ALGaAs==1))then
           write(2001,*) 2
        end if

        if(InGaAs_InAlAs==1)then     
           write(2001,*) 1
        end if
        if(Multimaterial==1)then     
           write(2001,*) 2
        end if

!**************************************************************

    write(2001,*) cob,cob1,cob2,cob3  ! conduction band offsets
    write(2001,*) nper*nlayers+1,0,MC_5reg
    write(2001,*) (lung_reg_act_layer(i),i=1,nper*nlayers+1)
    write(2001,*) fmin,fmax,nbnd,bz,dens_m_2
    write(2001,*) dtsim,tsimax,tstat           !nsubmx
    write(2001,*) ceemax
    write(2001,*) jread,jrecalc,jcorrente,jIFphonon,jinitial_cond,jread_ff
    write(2001,*) provv,nsubmx,fine_dop     
          
!***** Data for the input file of the MC to pass to Schroedinger********** 

           write(2002,*)  ll_high       !upper laser level
           write(2002,*)  ll_low        !lower laser level
           write(2002,*)  linj_inter    !injector level of interest
           write(2002,*)  lreg_act__low !active region ground level
           write(2002,*)  lreg_act_high !active region upper level
           write(2002,*)  nsubmx,nperiod(1),nperiod(2),nperiod(3),nperiod(4)  !# of energy levels considered in the MC
           do j=1,4
		     do i=1,nsubmx
              write(2001,*) indw(j,i)
             end do
           end do
		   do i=1,fine_dop
			  write(2001,*) (dope(i,k), k=1,4)
		   end do
		   write(2002,*) MC_5reg
		   write(2002,*) fine_dop
		   do i=1,fine_dop
			  write(2002,*) (dope(i,k), k=1,4)
		   end do
		   close(2001)
           close(2002)
         end program struct_2