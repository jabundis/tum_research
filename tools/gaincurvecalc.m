
% Biaslvls = [38 40 43 45 48 50 53]; %in kV/cm

nlvls = length(Biaslvls);
maindir = pwd; 


c0 = 2.99792458e8; % in m/s
c0_cm = c0*1E2; % cm /s

for vidx = 1:length(Biaslvls)
% figure; 
fold = [maindir '\' num2str(Biaslvls(vidx),'%d')];

erg = gain(fold,1:12,654,20e12,50e12); 
f = erg(1,:);
g = erg(2,:)/100;
lda  = c0_cm./f; % cm 

g_k = g; 
k = 2*pi./lda ; 

g_lda = fliplr(g);
lda  = fliplr(lda );

fname1 = ['gain_vs_f_' num2str(Biaslvls(vidx),'%d')];
fname2 = ['gain_vs_k_' num2str(Biaslvls(vidx),'%d')];
fname3 = ['gain_vs_lambda_' num2str(Biaslvls(vidx),'%d')];

minf = min(f); maxf = max(f); 
minlda = min(lda); maxlda = max(lda); mink = min(k); maxk = max(k); 
lda = lda.*1e4; 
f = f/1e12; 
write2file( fname1, f,g,['Gain versus frequency at ' num2str(Biaslvls(vidx),'%d') ': \r\n min freq=' num2str(minf) ' (THz) ; max freq = ' num2str(maxf) '(THz);' ],'freq (THz)','gain 1/cm');
write2file( fname2, k,g_k,['Gain versus wavenumber at ' num2str(Biaslvls(vidx),'%d') ': \r\n min k=' num2str(mink) ' (rad/cm) ; max k = ' num2str(maxk) ' (rad/cm) ;' ],'wavenum. (1/cm)','gain (1/cm)');
write2file( fname3, lda ,g_lda,['Gain versus wavelength at ' num2str(Biaslvls(vidx),'%d') ': \r\n min lda=' num2str(minlda) ' (um) ; max lda = ' num2str(maxlda) ' (um);'] ,'Wavelength (um)','gain 1/cm');
title(['Spectral gain at: ' num2str(Biaslvls(vidx),'%d') 'kV/cm'])
end


