% %%%% extract the IV characteristic of the laser

clear all; 
% sw = 1; 
sw = 2;  % for the tunneling file! 
nWF = 5*4; % 5 wfs pper period and 4 periods
offset = 30; % offset to start readimg the WFs from


clc; 


maindir = pwd; 

%current density in A/m^2
global sf; global sf_op;  global sf_ac; global sf_ee; global sf_imp; global sf_if;     

% extract  the energies in the following order 
% Injector1, Injector2 -> ULL -> LLL1 -> LLL2 -> DEPOP1 -> DEPOP2
% DEPOP 
% these indices are of the WFs with energies ordered in increasing order!
INJ1 = 12; INJ2 = 10; ULL = 11; LLL1=9; LLL2=8; DEPOP1 = 7;DEPOP2 = 5;

lvls = [INJ1 INJ2 ULL LLL1 LLL2 DEPOP1 DEPOP2]; 

W_sf = zeros(length(lvls)*length(lvls),1); 
W_sf_op = W_sf; W_sf_ac = W_sf; W_sf_ee = W_sf; W_sf_imp = W_sf; W_sf_if = W_sf; 


%get the current energies.. 
Ens = 0;
if sw == 1
    fid = fopen([pwd,'\input1.txt']);
else
    fid = fopen([pwd,'\inputt.txt']);
end
%skip through the first 31 lines 
for ctr = 1:offset
    tline = fgets(fid);
end
for ctr = 1:nWF
    tline = fgets(fid);
    mtx = str2num(tline);
    Ens(ctr) = mtx(1); 
    ctr;
end
fclose(fid);

Ens 
[sortedEns, order]  = sort(Ens);
Energy_lvls = sortedEns(lvls);
levs2get = order(lvls) 

for i=1:length(levs2get)
     for j = 1:length(levs2get)
         if i == j 
             continue
         end
            % scattering from level i to level j
            scan(pwd,levs2get(i), levs2get(j), 1:5,[],3);
            W_sf(j+length(levs2get)*(i-1)) = sf; 
            W_sf_op(j+length(levs2get)*(i-1)) = sf_op; 
            W_sf_ac(j+length(levs2get)*(i-1)) = sf_ac; 
            W_sf_ee(j+length(levs2get)*(i-1)) = sf_ee;
            W_sf_imp(j+length(levs2get)*(i-1)) = sf_imp; 
            W_sf_if(j+length(levs2get)*(i-1))= sf_if; 
     end
end

% each row now denotes the outscattering rates from the corresponding level
% to all other levels 
W_sf_mtx = transpose(reshape(W_sf,length(levs2get),length(levs2get)));
% since the ordering now is [INJ1 INJ2 ULL LLL1 LLL2 DEPOP1 DEPOP2]
% this means that for e.g. W_sf(1,3) will be the out-rate from INJ1 to ULL 

% different machanisms
W_sf_op_mtx = transpose(reshape(W_sf_op,length(levs2get),length(levs2get)));
W_sf_ac_mtx = transpose(reshape(W_sf_ac,length(levs2get),length(levs2get)));
W_sf_ee_mtx = transpose(reshape(W_sf_ee,length(levs2get),length(levs2get)));
W_sf_imp_mtx = transpose(reshape(W_sf_imp,length(levs2get),length(levs2get)));
W_sf_if_mtx = transpose(reshape(W_sf_if,length(levs2get),length(levs2get)));


save('rates.mat')