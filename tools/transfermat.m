function erg=transfermat(V,z,meff,E)
eps0=8.8541878176e-12; e0=1.60217646e-19; hbar=1.05457168e-34; me=9.10938188e-31; %m0=0.2*me; mL=0.067*me; mR=0.067*me;
global AB; global coeff;
meffE=meff;
n=length(V); AB=zeros(2,n); coeff=zeros(1,n);
k=sqrt(2*meffE.*(E-V))/hbar; D=diff(z);
if(sum(imag(k)<0)) pause;
    'branch!'
end;
%A=1, B=0 => psi(0)=1-i, psi'(0)=k(0)*(1+i);
kL=k(1); kR=k(end); mL=meffE(1); mR=meffE(end); k0=sqrt(2*me*e0)/hbar; kn=abs(k0); bL=kL/mL; bR=kR/mR;
if((imag(k0)<0)) pause;
    'branch!'
end;
%renorm for stepwise constant matrix methods
k=k/kn; D=kn*D; kL=kL/kn; kR=kR/kn; AB(:,1)=[0;1];

%transfer matrix 2: step centered
T2=[1 0;0 1];
for m=1:(n-1)
    k1=k(m); k2=k(m+1); b1=k(m)/meffE(m); b2=k(m+1)/meffE(m+1);
    %T0=[exp(i*k2*D/2) 0;0 exp(-i*k2*D/2)]*1/(2*b2)*[b2+b1 b2-b1;b2-b1 b2+b1]*[exp(i*k1*D/2) 0;0 exp(-i*k1*D/2)];
    T0=[0.5*(1+b1/b2)*exp(i*(k1+k2)*D(m)/2) 0.5*(1-b1/b2)*exp(i*(k2-k1)*D(m)/2);0.5*(1-b1/b2)*exp(i*(k1-k2)*D(m)/2) 0.5*(1+b1/b2)*exp(-i*(k1+k2)*D(m)/2)];
    T2=T0*T2;
    coeff(m)=k1*kn; AB(:,m+1)=T2*AB(:,1);
    %psi(m+1)=sum(T2*[1;-1]);
end;
erg=AB(2,end)/AB(1,end);
%psi=psi/sqrt(trapz(z,abs(psi).^2));