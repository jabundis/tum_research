function [] =tunnelall(dir,sw)
%set sw=1 to correct band bending
%all states per period are used for tight-binding basis
if(~exist('sw')) sw=0; end;
e0=1.60217646e-19; hbar=1.05457168e-34; me=9.10938188e-31; eps0=8.8541878176e-12; meff=0.067; epsr=13.18;
eef=load(strcat(dir,'\e_bound'));
nsub=length(eef); m=nsub/4;
ppot=load (strcat(dir,'\ppot1.dat'));
zv=ppot(1:(end-2),1); V=e0*ppot(1:(end-2),2)'; V0=V;
Epot=0;
for k=2:length(zv)
    if(zv(k)==zv(k-1)) if(Epot==0) Epot=abs(V(k)-V(k-1)); end;  V0(k:end)=V0(k:end)-V(k)+V(k-1); end;
end;
%V0=V0+Epot;
Vs=V-V0; Vs=Vs-max(Vs);
Vo=max(Vs)-min(Vs); z1=zv(diff(Vs)>0.9*Vo); z2=zv(diff(Vs)<-0.9*Vo); zb=(z1(1:(end-1))+z2(2:end))/2; bd=z2(2:end)-z1(1:(end-1)); %assumes that structure starts and ends with a barrier
ib=(length(zb)+1)/5; %barriers per period
[~,ind]=max(bd); ibm=mod(ind-1,ib)+1+ib; Vt=V+(V0-V).*((zv<zb(ibm))|(zv>zb(ibm+ib)))';
psief=load(strcat(dir,'\psi.dat'));
nz=length(psief(:,2))/(4*m);
z=psief(1:nz,1);
for n=1:nsub psiei(n,:)=psief(((n-1)*nz+1):(n*nz),2); psiei(n,:)=psiei(n,:)/sqrt(trapz(z,psiei(n,:).^2)); psi(n,:)=interp1(z,psiei(n,:),zv); end;
psi(isnan(psi))=0; %NaN caused by interp1 at z points outside of zv grid
for n=1:nsub psi(n,:)=psi(n,:)/sqrt(trapz(zv,psi(n,:).^2)); end;
%zs=trapz(z,z.*abs(psiel.*psieu))/trapz(z,abs(psiel.*psieu));
%hold off;

%Lperiod
fid = fopen([dir,'\struct_2.inp'], 'r');
fgetl(fid);
z1 = str2num(fgetl(fid)); z1=z1(1);
Lperiod=-z1/5;  %at the moment, the input structure contains 5 periods, and first layer is repeated a sixth time
fclose(fid);

%input1.txt
fid = fopen([dir,'\input1.txt'], 'r');
tun = fopen([dir,'\inputt.txt'], 'w');
tline = fgetl(fid); numz = str2num(tline); fprintf(tun,'%s\r\n',tline);
tline = fgetl(fid); zincr = str2num(tline); zincr=zincr*1.e-10; fprintf(tun,'%s\r\n',tline);
for k=1:8
    tline = fgetl(fid); fprintf(tun,'%s\r\n',tline);
end;
for k=1:nsub
    tline = fgetl(fid); fprintf(tun,'%s\r\n',tline);
end;
for k=1:nsub
	v=[]; v = str2num(fgetl(fid)); esub(k)=v(1); percre(1,k)=v(2); percre(2,k)=v(3); percre(3,k)=v(4); %read(19,*)esub(1,k),percre(1,1,k),percre(1,2,k),percre(1,3,k)
end;
for k=1:nsub
    for n=1:numz
	    v=[]; v = str2num(fgetl(fid)); poz(n)=v(1); zeta(k,n)=v(2); %read(19,*)(poz,zeta(1,k,n),i=1,numz)
    end;
end;
fclose(fid);
%end input1.txt
E=e0*esub';
Eperiod=abs(esub(3*m)-esub(2*m)); Ea=sum(E((m+1):(4*m)))/(3*m);

for ni=1:(4*m)
    for nf=1:(4*m)
        T(ni,nf)=trapz(zv,psi(ni,:).*(Vt-V).*psi(nf,:));
    end;
end;
T=(T+transpose(T))/2; %cure the small assymetry of T due to numerical errors
%[X,lam] = eigs(T+diag(E),m,'sa'); Etb=diag(lam);
[X,lam] = eig(T+diag(E)); Etb=diag(lam); %[Etb,ind] = sort(diag(lam)); Etb=Etb(1:m); X=X(:,ind(1:m));
wt=zeros(nsub,nsub); wd=zeros(nsub,nsub); ic=0; PL=ones(4*m,1)*NaN;
zeta2=zeta*0; esub2=esub*0; psis=psi*0;
[zvu,iu,~]=unique(zv); Es=zeros(m,1);
%select functions with smallest energy relative to conduction band
for ni=1:length(Etb)
    X(:,ni)=X(:,ni)/norm(X(:,ni)); psih(ni,:)=sum(diag(X(:,ni))*psi);
    Eh(ni)=Etb(ni)-trapz(zv,psih(ni,:).^2.*Vt); %relative energy with respect to tight-binding potential
    if(trapz(zv,psih(ni,:).^2.*(zv>=zb(ibm))'.*(zv<=zb(ibm+ib))')<0.5) Eh(ni)=NaN; end; %eliminate solutions which are not localized in the tight-binding module
    %Eh(ni)=sum(X(:,ni).^2.*E)-trapz(zv,psis(ni,:).^2.*V); %relative energy with respect to extended potential
end;
[~,ind] = sort(Eh); Etb=Etb(ind(1:m)); X=X(:,ind(1:m));
%%%%%%%%%%%%%
for ni=(m+1):(2*m)
    X(:,ni-m)=X(:,ni-m)/norm(X(:,ni-m)); psis(ni,:)=sum(diag(X(:,ni-m))*psi);
    Es(ni-m)=sum(X(:,ni-m).^2.*E);
    for k=[-1 1 2]
        ni1=ni+k*m;
        psis(ni1,:)=interp1(zvu,psis(ni,iu),zv-k*Lperiod,'v5cubic'); psis(ni1,isnan(psis(ni1,:)))=0;
        %esub2(ni1)=Etb(nim)/e0+k*Eperiod; %E1+k*Eperiod;
    end;
end;

%Poisson
if(sw)
    ave=load([dir,'\fe.dat']); ind=1:m;
    Ek=ave(1:(length(ave(:,1))),1)*e0; dis1=ave(1:(end-0),1+m+ind); dis2=ave(1:(end-0),1+2*m+ind); dis0=(dis1+dis2)/2;
    n2D=me*meff/pi/hbar^2*trapz(Ek,dis0(:,1:m));
    rho=-e0*sum(diag([n2D n2D n2D n2D])*psis(:,iu).^2);
    %%%%%%%%%%%load doping data; only valid for a single doping region per period
    filename=[dir,'\struct_2.inp']; fid2=fopen(filename,'r');
    net=str2num(fgets(fid2)); %total # of layers
    zd=[]; while(length(zd)<net) zd=[zd str2num(fgets(fid2))]; end; %coord. interfaces
    conc=[]; while(length(conc)<(net+1)) conc=[conc str2num(fgets(fid2))]; end; %Al concentration
    tline1=fgets(fid2); tline2=fgets(fid2); tline3=fgets(fid2); tline4=fgets(fid2); tline5=fgets(fid2);
    z_min=num2str(tline1); DeltaLambda=str2num(tline2); Delta=DeltaLambda(1); Lambda=DeltaLambda(2); tl=str2num(tline5); bige=0.0012*(tl+50)*e0;
    tline1=fgets(fid2); tline2=fgets(fid2); tline3=fgets(fid2); tline4=fgets(fid2); tline5=fgets(fid2); %epsr=str2num(tline3);
    dummy=[]; while(length(dummy)<12) dummy=[dummy str2num(fgets(fid2))]; end; %coord. interfaces
    tline1=fgets(fid2); tline2=fgets(fid2); tline3=fgets(fid2); tline4=fgets(fid2); tline5=fgets(fid2);
    cobv=str2num(tline5); cob=cobv(1);
    while(1)
        tline=fgets(fid2);
        if(ischar(tline))
            tline1=tline2; tline2=tline3; tline3=tline4; tline4=tline5; tline5=tline;
        else break;
        end;
    end;
    fclose(fid2);
    dop=zeros(5,4); dop(1,:)=str2num(tline1); dop(2,:)=str2num(tline2); dop(3,:)=str2num(tline3); dop(4,:)=str2num(tline4); dop(5,:)=str2num(tline5);
    dop(:,3)=dop(:,3)*1e6; dop(:,1:2)=min(zv)+dop(:,1:2);
    nD=0*zv;
    for n=1:5 nD=nD+(zv>=dop(n,1)).*(zv<dop(n,2))*dop(n,3); end; nD=nD*e0;
    %%%%%%%%%%%%%%%%%%%%%%%%%%
    rho=-rho*(trapz(zvu,nD(iu))/5)/(trapz(zvu,rho)/4);
    zr=zb(ibm):(zvu(2)-zvu(1)):zb(ibm+ib); rho=interp1(zvu,rho'+nD(iu),zr,'v5cubic');
    eps=eps0*epsr; dz=(zr(2)-zr(1))*1e-10;
    M=zeros(length(zr),length(zr));
    for mz=1:length(zr)
        M(mz,mz)=-2/dz^2;
        if(mz>1) M(mz,mz-1)=1/dz^2; end;
        if(mz<length(zr)) M(mz,mz+1)=1/dz^2; end;
    end;
    DVh=transpose(M\(e0/eps*rho'));
    zr2=[zr-2*Lperiod zr-Lperiod zr zr+Lperiod zr+2*Lperiod zr+3*Lperiod]; DVh=[DVh DVh DVh DVh DVh DVh]; [zr2,ind,~]=unique(zr2); DV=interp1(zr2,DVh(ind),zv,'v5cubic');
    Vq=V0-zv'*(V0(end)-V0(1))/(zv(end)-zv(1)); Vq(1)=Vq(2); DV=DV'-Vq; DV=DV-mean(DV);
end;
%%%%%%%%%%%%%%%%%%%%%%%%%%

for ni=(m+1):(2*m)
    for nf=(2*m+1):(3*m)
        nih=ni; nfh=nf; nim=mod(ni-1,m)+1; nfm=mod(nf-1,m)+1;
        d=Etb(nfm)+e0*Eperiod-Etb(nim);
        t=0; np=ceil(nf/m)-ceil(ni/m); %np is difference in period
        %t=sum(X((m+1):(4*m),nim).*X(1:(3*m),nfm).*(E((m+1):(4*m))-Ea)); %tunneling to right-neighbouring well
        tr=trapz(zv,psis(ni,:).*(V-Vt).*psis(nf,:));
        tl=trapz(zv,psis(ni-m,:).*(V-Vt).*psis(nf-m,:));
        for k=(-3):3
            if((ni-k*m>0)&(ni-k*m<=nsub)&(nf-k*m>0)&(nf-k*m<=nsub))
                %wt(ni-k*m,nf-k*m)=abs(t)/hbar; wt(nf-k*m,ni-k*m)=abs(t)/hbar;
                wt(ni-k*m,nf-k*m)=sqrt(abs(tl*tr))/hbar; wt(nf-k*m,ni-k*m)=wt(ni-k*m,nf-k*m);
                wd(ni-k*m,nf-k*m)=abs(d)/hbar; wd(nf-k*m,ni-k*m)=abs(d)/hbar;
                %de(ni-k*m,nf-k*m)=deh2/e0; de(nf-k*m,ni-k*m)=deh2/e0;
            end;
        end;
    end;
end;
for ni=(m+1):(2*m)
    zeta2(ni,:)=sum(diag(X(:,ni-m))*zeta); nim=mod(ni-1,m)+1;
    for k=(-3):3
        ni1=ni+k*m;
        if((ni1>0)&(ni1<=nsub))
            zeta2(ni1,:)=interp1(poz,zeta2(ni,:),poz-k*Lperiod,'v5cubic'); zeta2(ni1,isnan(zeta2(ni1,:)))=0;
            %if((nf1>0)&(nf1<=nsub)) psis(ni1,:)=(al*psi(ni1,:)+bl*psi(nf1,:))/sqrt(al^2+bl^2); end;
            %esub2(ni1)=Etb(nim)/e0+k*Eperiod; %E1+k*Eperiod;
            esub2(ni1)=Es(nim)/e0+k*Eperiod; %E1+k*Eperiod;
            if(sw) esub2(ni1)=esub2(ni1)+trapz(psis(nim+m,:).^2.*DV)/e0; end;
        end;
    end;
end;

for k=1:nsub
	fprintf(tun,'%E %E %E %E\r\n',esub2(k),percre(1,k),percre(2,k),percre(3,k));
end;
for k=1:nsub
    for n=1:numz
	    fprintf(tun,'%d %E\r\n',poz(n),zeta2(k,n));
    end;
end;
for ni=1:nsub
    for nf=1:nsub
	    fprintf(tun,'%E %E ',wt(ni,nf),wd(ni,nf));
    end;
	fprintf(tun,'\r\n');
end;
fclose(tun);

%ha=plot(zv,psi.*(psi~=psis)*Epot/max(max(abs(psi)))); set(ha,'LineWidth', 2);
%ha=plot(zv,psis.*(psi~=psis)*Epot/max(max(abs(psis)))); set(ha,'LineWidth', 2,'LineStyle',':');
vPL=(PL+1).*(1:length(PL))'; vPL=vPL(~isnan(vPL));
% if(length(vPL)>0)
%     A=[0 0 1;0 0.5 0;1 0 0;0 0.75 0.75; 0.75 0 0.75;0.75 0.75 0; 0.25 0.25 0.25];
%     set(groot,'defaultAxesColorOrder',A)
%     Psi2=(PL+esub')*ones(1,length(psi(1,:)))+psi.^2*Epot/e0/max(max(abs(psi.^2))); Psi2=Psi2(vPL,:);
%     Psi2s=(PL+esub2')*ones(1,length(psis(1,:)))+psis.^2*Epot/e0/max(max(abs(psi.^2))); Psi2s=Psi2s(vPL,:);
%     figure; ha=plot(zv,Psi2); set(ha,'LineWidth', 2); legend(num2str(vPL)); hold on;
%     ax = gca; ax.ColorOrderIndex = 1;
%     ha=plot(zv,Psi2s,'--'); set(ha,'LineWidth', 2);
%     ha=plot(zv,V/e0,'k:'); set(ha,'LineWidth', 2);
% end;
%ha=plot(zv,V,'k:',zv,psi1s.^2*Epot/max(psi1s.^2),'r',zv,psi2s.^2*Epot/max(psi2s.^2),'b',zv,psi1.^2*Epot/max(psi1.^2),'m--',zv,psi2.^2*Epot/max(psi2.^2),'g--');
%ha=plot(zv,V,'k:',zv,psi1s*Epot/max(abs(psi1s)),'r',zv,psi2s*Epot/max(abs(psi2s)),'b',zv,psi1*Epot/max(abs(psi1)),'m--',zv,psi2*Epot/max(abs(psi2)),'g--'); set(ha,'LineWidth', 2);