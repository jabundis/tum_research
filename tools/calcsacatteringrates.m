dir = pwd; 
lvls = [12 10 9 7]

Tmtx = zeros(length(lvls)); 
Wmtx = Tmtx; 

for i=1:length(lvls)
    for j = 1:length(lvls) 
    global sf; 
    scan(dir,lvls(i), lvls(j), 1:5,[],3);
    Tmtx(i,j) = 1/sf; 
    Wmtx(i,j) = sf; 
    end
end