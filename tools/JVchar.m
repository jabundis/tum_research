

Biaslvls = {'4','5','6','8','9','10','10.1','10.2','10.3','10.4','11','12','13'}; %in kV/cm

nlvls = length(Biaslvls);
V = [] ;

for vidx = 1:nlvls
    V(vidx) = str2num(Biaslvls{vidx});
end

maindir = pwd;

%current density in A/m^2
J = []; 
for vidx = 1:nlvls  
    dir = [maindir '\' Biaslvls{vidx}];
%     figure; 
    [err,time, Itmp] = currPETZ(dir,100,5E4,1); 
    J(vidx) = mean(Itmp);
end

figure; 
plot(V,J,'-xr','Linewidth',2.0);
xlabel('V (kV/cm)'); ylabel('J(A/m^2)'); 




