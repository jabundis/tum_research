function ga=gam(ni,nf,path,sw)
%sw=0: dephasing rates for tunneling; sw=1: dephasing rates other transitions (includes tunneling rate broadening)
if(~exist('sw')) sw=0; end;
m=length(load(strcat(path,'\e_bound')))/4; ch=1:m; [dummy,ind] = sort(ch);
nih=mod(ni-1,m)+1; nfh=mod(nf-1,m)+1;
ave=load([path,'\fe.dat']);
E=ave(1:(length(ave(:,1))),1);
dis1=ave(1:(end-0),1+m+ind); dis2=ave(1:(end-0),1+2*m+ind); dis=(dis1+dis2)'/2;
dif=abs(dis(nih,:)-ones(length(ni),1)*dis(nfh,:));
gamp=load([path,'\puredep.dat']);
gamh=gamp(ni+4*m*(nf-1),:);
gp=sum((gamh.*dif)')./sum(dif'); if(nih==nfh) gp=sum((gamh.*dis(nih,:))')./sum(dis(nih,:)'); end;
figure; plot(E,gamh,E,dif*max(gamh)/max(dif)); xlabel('Ekin/eV'); ylabel('gp [ps^{-1}], population difference [a. u.]');
if(sw==0) str='/gamt.dat'; else str='/gain.dat'; end;
fid=fopen([path,str]);
if(fid~=-1)
  fclose(fid);
  A=load([path,str]); puredep=gamp;
  E=A(:,1); h=(A(:,(2*m+2):(4*m+1))+A(:,(4*m+2):(6*m+1)))/2; n=h(:,1:2:(2*m)); p=h(:,2:2:(2*m)); %3d density of particles in a state n in energy cell around E(i) is given by A(i,n+1)*|psi_nsu|^2
  tun = fopen([path,'\inputt.txt'], 'r');
  tline = fgetl(tun); nz = str2num(tline); 
  tline = fgetl(tun); zincr = str2num(tline); zincr=zincr*1.e-10;
  for k=1:8
    tline = fgetl(tun);
  end;
  for k=1:(4*m)
    tline = fgetl(tun);
  end;
  for k=1:(4*m)
	tline = fgetl(tun); dummy = str2num(tline); eef(k)=dummy(1);
  end;
  for k=1:(4*m)
    for k2=1:nz
	    tline = fgetl(tun); dummy = str2num(tline); z(k2)=dummy(1); psief(k2,k)=dummy(2); z=z';
    end;
  end;
  fclose(tun);
  %%%%%%%%%%%%%%
  ez=zeros(4*m,4*m); wif=zeros(4*m,4*m);
  %wmin=min(min(wif.*(1+0./wif))); wmax=max(max(wif));
  nih=mod(ni-1,m)+1; nfh=mod(nf-1,m)+1;
  for nE=(1:length(E))   
      if(n(nE,nih)*p(nE,nih)==0) if(nE>1)gi=gi_old; else gi=0; end; else gi=(p(nE,nih)/n(nE,nih)); end;
      if(n(nE,nfh)*p(nE,nfh)==0) if(nE>1)gf=gf_old; else gf=0; end; else gf=(p(nE,nfh)/n(nE,nfh)); end;
      gi_old=gi; gf_old=gf;
      %gamma=(gi+gf)/2+puredep(4*m*(ni-1)+nf,nE); %assumption that in stationary case, in- and out-scattering rates are equal (if we only consider intersubband scattering, this does not hold for each individual transition! However, the numerical example GaAs3.4_15test yielded practically identical results for the spectral gain obtained based on in- and outscattering)
      gamma(nE)=(gi+gf)/2; %assumption that in stationary case, in- and out-scattering rates are equal (if we only consider intersubband scattering, this does not hold for each individual transition! However, the numerical example GaAs3.4_15test yielded practically identical results for the spectral gain obtained based on in- and outscattering)
      giv(nE)=gi; gfv(nE)=gf; 
  end;
  gi=trapz(E,giv'.*n(:,nih))/trapz(E,n(:,nih));
  gf=trapz(E,gfv'.*n(:,nfh))/trapz(E,n(:,nfh));
  disp(['total: ',num2str(((gi+gf)/2+gp)/1e12),' ps^(-1), (gi+gf)/2=',num2str((gi+gf)/2/1e12),' ps^(-1), gp=',num2str(gp/1e12),' ps^(-1)']);
  ga=[(gi+gf)/2+gp,(gi+gf)/2,gp];
end;