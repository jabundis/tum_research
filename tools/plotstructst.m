%**************************************************************************
%
% This programm plots the structure of the conduction band edge and the
% squared absolute values of the wavefunctions for the calculated bound
% state energy levels
%
% FUNCTION []=
%         PLOTSTRUCT(DIRECTORY)
%
%DIRECTORY....................the directory in the home directory in which
%.............................the structure files are stored 
%.............................(e.g. 'Project\QCL')
%
%**************************************************************************

function [] =...
	plotstructst(k,dir)
%k=1: input1; k=2: inputt
e0=1.60217646e-19;
Escale=0.025; m=length(load(strcat(dir,'\e_bound')))/4;
ppot=load (strcat(dir,'\ppot1.dat'));
plot(ppot(:,1)/10,ppot(:,2),'Color',[0 0 0],'linestyle','--','linewidth',2);
hold on;
%%%%
  if(k==1) tun = fopen([dir,'\input1.txt'], 'r'); end;
  if(k==2) tun = fopen([dir,'\inputt.txt'], 'r'); end;
  if(k==3) tun = fopen([dir,'\inputt1.txt'], 'r'); end;
  tline = fgetl(tun); nz = str2num(tline); 
  tline = fgetl(tun); zincr = str2num(tline); zincr=zincr*1.e-10;
  for n=1:8
    tline = fgetl(tun);
  end;
  for n=1:(4*m)
    tline = fgetl(tun);
  end;
  for n=1:(4*m)
	tline = fgetl(tun); dummy = str2num(tline); eef(n)=dummy(1);
  end;
  for n=1:(4*m)
    for k2=1:nz
	    tline = fgetl(tun); dummy = str2num(tline); z(k2)=dummy(1); psief(k2,n)=dummy(2); z=z';
    end;
  end;
  fclose(tun);
ma=max(max(psief.^2));
%%%%%%
%psel=load (strcat(dir,'\psel',num2str(k),'.dat'));
nsub=4*m;
npt=nz;
hold on;
b=1:nsub;
lb=size(b);
A =[0,0,1;0,0.5,0;1,0,0;0,0.75,0.75;0.75,0,0.75;0.75,0.75,0;0.25,0.25,0.25;0,1,0;0.5,0.5,0.5;0.5,0.5,0;1,0.5,0];
A=A(1:m,:);
set(gca,'ColorOrder',A);
for j=1:nsub
   x(:,j)=z/10; E(:,j)=psief(:,j).^2/ma*Escale+eef(j);
end;
B =[0,0,1;0,0.5,0;1,0,0;0,0.75,0.75;0.75,0,0.75;0.75,0.75,0;0.25,0.25,0.25;0,1,0;0.5,0.5,0.5;0.5,0.5,0;1,0.5,0];
A=B(1:m,:);
set(gca,'ColorOrder',A);
wa=E;
if(k==1) plot(x,wa,'-','linewidth',2); end;
if(k==2) plot(x,wa,'-','linewidth',2); end;
if(k==3) plot(x,wa,'-','linewidth',2); end;

hold on;
xlabel('z/nm'); %'Position x [nm]'); %
ylabel('E/eV');  %'Energy [eV]'); %
%for j=1:la(2)
%    plot(psel2((a(j)-1)*203+1:a(j)*203,1),psel2((a(j)-1)*203+1:a(j)*203,2),'--');%'Color',[0.6 0 0]);
%    hold on;
%end;

%this_handle2 = figure;
%    set(this_handle2, 'defaultaxesfontsize', 20);
%    set(this_handle2, 'DefaultLineLineWidth', 2);
%plot(ppot1(:,1),ppot1(:,2));
%axis([-606 0 0 0.8]);
%hold on;
%for j=1:lb(2)
%    plot(psel1((b(j)-1)*203+1:b(j)*203,1),psel1((b(j)-1)*203+1:b(j)*203,2),'Color',[0 0.6 0]);
%    hold on;
%end;